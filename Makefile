CONNECTIONS := 100
SECONDS     := 180
URL         := http://benchmarked:5000/
FLAGS       :=

CADDY  := caddy_v0.10.6_linux_amd64.tar.gz
ELIXIR := erlang-solutions_1.0_all.deb
RACKET := racket-6.10-x86_64-linux.sh

CADDY_URL  := https://github.com/mholt/caddy/releases/download/v0.10.6/$(CADDY)
RACKET_URL := https://mirror.racket-lang.org/installers/6.10/$(RACKET)

ENV := env 'CONNECTIONS=$(CONNECTIONS)' 'SECONDS=$(SECONDS)' 'URL=$(URL)' \
           'FLAGS=$(FLAGS)'

RESULT_TARGETS := $(shell awk '/^results\// {gsub(/:/, ""); print $$1}'\
                          Makefile)

all: run


vendor/$(CADDY):
	cd vendor && wget -c "$(CADDY_URL)"

vendor/$(RACKET):
	cd vendor && wget -c "$(RACKET_URL)"

vendor: vendor/$(CADDY) vendor/$(ELIXIR) vendor/$(RACKET)
	cd vendor && sha256sum -c SHA256SUMS

clean-vendor:
	-rm vendor/$(CADDY)
	-rm vendor/$(RACKET)


build: vendor
	docker build --compress --file Dockerfile.ab --tag rvsw/ab .
	docker build --compress --file Dockerfile.benchmarked \
	             --tag rvsw/benchmarked .

clean-docker:
	-docker rm $(docker stop $(docker ps --all \
		                                 --filter ancestor=rvsw/benchmarked \
	                                     '--format={{.ID}}' --quiet))
	docker rmi --force rvsw/ab
	docker rmi --force rvsw/benchmarked


network:
	-docker network rm rvsw
	docker network create rvsw

clean-network:
	-docker network rm rvsw


results/stateful.txt:
	$(ENV) ./benchmark.sh "$@" racket-servlet/
results/stateless.txt:
	$(ENV) ./benchmark.sh "$@" racket-servlet/ --stateless
results/scgi.txt:
	$(ENV) ./benchmark.sh "$@" racket-scgi/
results/custom-single.txt:
	$(ENV) ./benchmark.sh "$@" racket-custom/ single
results/custom-many.txt:
	$(ENV) ./benchmark.sh "$@" racket-custom/ many
results/custom-places.txt:
	$(ENV) ./benchmark.sh "$@" racket-custom/ places
results/custom-many-places.txt:
	$(ENV) ./benchmark.sh "$@" racket-custom/ many-places
results/caddy.txt:
	$(ENV) ./benchmark.sh "$@" caddy/
results/compojure.txt:
	$(ENV) ./benchmark.sh "$@" clojure-compojure/
results/flask.txt:
	$(ENV) ./benchmark.sh "$@" python-flask/
results/guile.txt:
	$(ENV) ./benchmark.sh "$@" guile-web-server/
results/plug.txt:
	$(ENV) ./benchmark.sh "$@" elixir-plug/
results/sinatra.txt:
	$(ENV) ./benchmark.sh "$@" ruby-sinatra/

clean-results:
	-rm results/*.txt

show-results-brief:
	grep 'Requests per second' results/*
show-results-long:
	grep -A 29 'Concurrency Level' results/*


run: build network $(RESULT_TARGETS) show-results-brief clean-network


.PHONY: all clean-docker clean-results clean-vendor network run vendor
