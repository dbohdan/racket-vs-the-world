require 'sinatra/base'

class LoremIpsum < Sinatra::Base
  disable :logging

  MESSAGE = """\
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis molestie
metus et ornare ultricies. Vestibulum magna magna, luctus a laoreet
vitae, dignissim at velit. Quisque imperdiet odio in diam blandit,
vitae bibendum ipsum pharetra. Nunc non consectetur lectus. Vivamus
orci massa, tincidunt non dui eget, varius iaculis mi. Sed condimentum
et odio ut efficitur. Nunc massa risus, porta non ante id, facilisis
tempor nisi. Aenean gravida sollicitudin viverra. Quisque vel ipsum eu
purus scelerisque lacinia. Pellentesque justo est, tincidunt sit amet
aliquam viverra, sagittis a urna. Ut in eros volutpat, vehicula urna
ut, gravida neque. Nulla eget blandit eros.

Sed sit amet libero eget ipsum vestibulum facilisis in in magna. Fusce
odio orci, hendrerit ac nisl id, tincidunt dignissim nisi. Sed
dignissim dolor vel scelerisque rhoncus. Vestibulum ac tellus tempus,
rhoncus velit varius, dictum enim. Sed vitae maximus metus, a finibus
augue. In facilisis malesuada maximus. Nullam est lectus, vehicula et
massa nec, tincidunt luctus est. Quisque est tellus, congue nec
fermentum eu, tempus ut est. Pellentesque cursus justo et ante
iaculis, eget vulputate nunc dictum. Proin odio sapien, condimentum in
pretium quis, dictum non lacus. Etiam vel sagittis augue, nec
consequat arcu.

Mauris ac massa vitae sapien placerat pulvinar in tristique ligula.
Aliquam libero mauris, imperdiet in est ut, maximus vestibulum dui.
Nulla et mi a enim pellentesque placerat a sit amet neque. Nunc ac
faucibus mauris, eget ultricies tortor. Mauris eu lacus nec augue
malesuada interdum. Phasellus a dui venenatis, tincidunt orci
dignissim, mattis orci. Nulla posuere ante venenatis sagittis aliquet.
Proin laoreet ornare venenatis. Nullam tincidunt odio mi, ut maximus
tellus malesuada ac. Donec consectetur dolor eget nibh viverra luctus.
Vestibulum lectus sem, varius eu risus a, efficitur pellentesque sem.
Donec laoreet, tellus eu maximus varius, magna ligula sodales sapien,
ac maximus dolor purus in dolor. Pellentesque habitant morbi tristique
senectus et netus et malesuada fames ac turpis egestas. Phasellus
accumsan ipsum vitae tincidunt facilisis. Pellentesque metus magna,
lobortis ornare augue nec, hendrerit tempus magna. Quisque vulputate
vehicula nisl, eleifend malesuada risus consectetur quis.

Fusce mattis enim et bibendum congue. Sed euismod enim at nunc
sollicitudin lacinia. Quisque blandit ligula et sem vestibulum
pulvinar. Nulla porta sollicitudin nunc porttitor ullamcorper.
Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
posuere cubilia Curae; Ut tincidunt sem ante, eget lacinia lectus
tincidunt ut. Donec nec sollicitudin tortor, non rhoncus dolor. Fusce
a diam ac diam condimentum luctus laoreet ut nunc. Etiam rutrum lectus
magna, eget ornare justo feugiat id. Interdum et malesuada fames ac
ante ipsum primis in faucibus. Pellentesque vel ex ac libero aliquam
luctus eget et leo. Curabitur eget augue efficitur, sagittis erat vel,
scelerisque urna. Fusce vel justo sed nunc malesuada convallis id
viverra magna. Phasellus tincidunt, nibh non accumsan tincidunt, est
justo efficitur erat, ut tempor lorem quam ut diam. Nulla vehicula
lorem non ex tincidunt laoreet. Suspendisse potenti.

Cras eu elit vitae nibh sodales scelerisque quis ac diam. Nulla id
ante posuere nibh consequat tincidunt. Vivamus ac leo vitae ipsum
ornare elementum. Integer vitae euismod libero, egestas interdum sem.
In nec lorem nibh. Curabitur vel erat eget justo ultricies tristique
nec nec diam. Duis eget aliquet eros. Morbi scelerisque, libero non
consequat tincidunt, est tortor finibus enim, dignissim suscipit
turpis magna vel nisl. Morbi ut purus id lectus tincidunt pharetra.
Maecenas aliquam erat eu urna malesuada vestibulum. Maecenas velit
urna, pulvinar quis ultricies in, consequat non leo. In ultrices
sapien nec ligula facilisis ultrices a sit amet nisl. In id auctor
lectus. Proin sollicitudin, erat non pharetra ultricies, augue metus
sodales massa, id tincidunt nulla arcu id nunc. Nulla facilisi. In hac
habitasse platea dictumst. Praesent ac eros et tortor posuere.
"""

  get '/' do
    content_type 'text/plain'
    MESSAGE
  end
end
