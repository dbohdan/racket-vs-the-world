#! /bin/sh

# Setup.
curl -o lein \
     https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein
chmod +x lein
LEIN_SILENT=true ./lein self-install
./lein ring uberjar

# Run.
java -jar target/lorem-ipsum-0.1.0-SNAPSHOT-standalone.jar
