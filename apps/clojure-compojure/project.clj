(defproject lorem-ipsum "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.6.0"]]
  :aot [lorem-ipsum.core]
  :main lorem-ipsum.core
  :plugins [[lein-ring "0.12.1"]]
  :ring {:handler lorem-ipsum.core/app
         :port 5000})
