#! /bin/sh
export LC_ALL=C.UTF-8
pip3 install -r requirements.txt
env FLASK_APP=lipsum.py python3 -m flask run --host=0.0.0.0
