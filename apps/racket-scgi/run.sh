#! /bin/sh

# Setup.
mkdir -p /tmp/nginx/proxy/
raco pkg install --auto scgi

# Run.
if which nginx; then
    nginx=nginx
elif [ -e /usr/sbin/nginx ]; then
    nginx=/usr/sbin/nginx
else
    echo "error: can't find nginx"
    exit 1
fi
racket --script scgi.rkt &
racket_pid="$!"
sleep 2
"$nginx" -c "$(pwd)/nginx.conf" &
nginx_pid="$!"
trap "kill \"$nginx_pid\" \"$racket_pid\" || true; exit" EXIT INT TERM

while true; do
    sleep 1
done
