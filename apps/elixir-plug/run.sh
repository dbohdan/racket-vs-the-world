#! /bin/sh
export LC_ALL=C.UTF-8
mix local.hex --force
mix local.rebar --force
mix deps.get
mix run --no-halt
