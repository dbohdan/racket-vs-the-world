defmodule LoremIpsum do
  use Application
  require Logger

  def start(_type, _args) do
    children = [
      Plug.Adapters.Cowboy.child_spec(:http, LoremIpsum.LoremIpsumPlug,
                                      [], port: 5000)
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
