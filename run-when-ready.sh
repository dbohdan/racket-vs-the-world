#! /bin/sh
set -eu

correct=/var/run-when-ready/correct.txt
actual=/tmp/actual.txt
max_tries=60

url="$1"
shift

tries=0
while [ "$tries" -lt "$max_tries" ]; do
    echo 'Waiting for the server to start up...'
    sleep 2
    curl -o "$actual" -s "$url" && break || true
    tries=$((tries + 1))
done
if [ "$tries" -eq "$max_tries" ]; then
    echo 'Error: Server unavailable after maximum number of tries'
    exit 1
fi

if [ -e "$actual" ]; then
    # Ensure we're getting the desired response.
    diff "$correct" "$actual"
else
    echo "Error: didn't get a response from the server."
    exit 1
fi

"$@"
