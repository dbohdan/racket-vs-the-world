#! /bin/sh
set -eu

output="$1"
app="$2"
shift 2

trap "docker kill ab benchmarked || true" EXIT INT TERM

echo "=== Benchmarking $(basename "$output" .txt)"
docker run --cpuset-cpus 0 --memory 768m --memory-swap -1 --name benchmarked \
           --net rvsw --rm --workdir "/apps/$app" \
           rvsw/benchmarked sh run.sh "$@" &
touch "$output"
tail -f "$output" &
exit_status=0
if ! docker run --cpuset-cpus 1 --memory 128m --memory-swap -1 --name ab \
                --net rvsw --rm $FLAGS \
                rvsw/ab run-when-ready "$URL" ab -c "$CONNECTIONS" \
                                                 -t "$SECONDS" \
                                                 -n 10000000 \
                                                 "$URL" > "$output"
then
    rm "$output"
    exit_status=1
fi
if docker stop benchmarked --time 1; then
    sleep 2
    trap - EXIT INT TERM
fi
exit "$exit_status"
