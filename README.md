# Racket vs. the World

**RvsW** is a benchmark comparing the performance of a trivial Web application implemented with different Web stacks. The application returns a circa 4 KiB blob of `text/plain` over HTTP/1.1. The benchmark pits [Racket](http://racket-lang.org/)'s stateless and stateful [servlets](https://docs.racket-lang.org/web-server/) against

* The [SCGI package](https://docs.racket-lang.org/scgi/) for Racket with [nginx](https://nginx.org/en/) as a reverse proxy;
* the [Caddy](https://caddyserver.com/) Web server;
* [Clojure](https://clojure.org/) and [Ring](https://github.com/ring-clojure/ring)/[Compojure](https://github.com/weavejester/compojure);
* [Python](https://www.python.org/) 3 and [Flask](http://flask.pocoo.org/);
* [GNU Guile](https://www.gnu.org/software/guile/)'s [`web server`](https://www.gnu.org/software/guile/manual/html_node/Web-Server.html) module;
* [Elixir](http://elixir-lang.org/) and [Plug](https://github.com/elixir-plug/plug);
* [Ruby](https://www.ruby-lang.org/en/) and [Sinatra](http://www.sinatrarb.com/).


## Running the benchmark

You will need around 2 GiB of disk space to build the Docker containers used for benchmarking. You will also need 1 GiB of free RAM and at least 2 CPU cores. It is recommended that you disable swapping on the host machine for the duration of the benchmark (`sudo swapoff -a`; you can re-enable it later with `sudo swapon -a`).

Install [Docker](https://docs.docker.com/engine/installation/), wget and GNU make on a Linux host (macOS hasn't been tested and may or may not work). Clone this repository and go to the repository directory in your shell. To run the whole benchmark, which takes 35–50 minutes, type the command

```sh
make
```

To run, e.g., two select benchmarks for one minute each, type

```sh
# The following step is only necessary if you haven't built the containers yet
# or have changed Dockerfile.* or something in apps/ since the last build.
make build

rm results/stateless.txt results/guile.txt
make SECONDS=60 network results/stateless.txt results/guile.txt clean-network
```


## Caveats

Take benchmarks, especially new, small artificial benchmarks, with a grain of salt. They may be inaccurate for a number of reasons and aren't straightforward to extrapolate to the performance of more complex real-world applications. Is it worth reading [On HTTP Load Testing](https://www.mnot.net/blog/2011/05/18/http_benchmark_rules) and its [HN discussion](https://news.ycombinator.com/item?id=2559079).


## License

MIT.
